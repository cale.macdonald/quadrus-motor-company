## Install

1. Clone the Repository or download the zip file
```
$ git clone https://gitlab.com/cale.macdonald/quadrus-motor-company.git
```
2. Using Visual Studio 2015 open the .sln file in the newly created directory
3. Run the project by pressing CTRL+F5
    * This will restore missing nuget packages

## Features
1. Gallery
    * The gallery is available to anyone
    * A basic search has been added which searches Make, Mode, and Description fields
    * A listing of available brands is added on the side, which will filter the list shown when clicked
    * A link to each vehicle's additional details can be viewed by clicking on the vehicle of interest
2. Vehicle Details
    * The basic details of the vehicle are shown with stubs for the following
        * Product Description
        * Additional Information
        * Reviews
    * Availability is always available (a further enhancement would be to keep track of current inventory count)
3. Inventory Management (Only available once authenticated by using Login)
    * Edit price and description of all vehicles
    * Remove unwanted vehicles
    * Add new vehicles
        * Make and model are checked on add to ensure no duplicates are added

## Future Enhancements
* Add a proper landing page
* Add vehicle inventory counts and use to determine if a vehicle is available for purchase
* Adding vehicle ordering
* Additional vehicle fields such as color, milage, condition (new / used) which all could be searchable
* Shopping Cart
* Notify when vehicle is available (if out of stock)
* Logging

### Security Considerations
* No validation is currently being done on uploaded images which for a commercial project would need to be added
