﻿namespace QuadrusMotorCompany.Command
{
    public interface ICommandValidator<in TCommand>
        where TCommand : ICommand
    {
        CommandValidationResult Validate(TCommand command);
    }
}