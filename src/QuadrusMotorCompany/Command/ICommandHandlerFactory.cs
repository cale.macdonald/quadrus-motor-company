﻿namespace QuadrusMotorCompany.Command
{
    public interface ICommandHandlerFactory
    {
        ICommandHandler<TCommand> Create<TCommand>() where TCommand : ICommand;
    }
}