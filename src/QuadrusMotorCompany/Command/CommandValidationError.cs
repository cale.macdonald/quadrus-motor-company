namespace QuadrusMotorCompany.Command
{
    public class CommandValidationError
    {
        public CommandValidationError(string property, string message)
        {
            Property = property;
            Message = message;
        }

        public string Property { get; }

        public string Message { get; }
    }
}