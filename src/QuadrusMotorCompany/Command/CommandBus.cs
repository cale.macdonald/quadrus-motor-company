namespace QuadrusMotorCompany.Command
{
    public class CommandBus : ICommandBus
    {
        private readonly ICommandHandlerFactory _factory;
        private readonly ICommandValidatorFactory _validatorFactory;

        public CommandBus(ICommandHandlerFactory factory, ICommandValidatorFactory validatorFactory)
        {
            _factory = factory;
            _validatorFactory = validatorFactory;
        }

        public CommandValidationResult Execute<TCommand>(TCommand command) where TCommand : ICommand
        {
            var aggregateResult = Validate(command);
            if (aggregateResult.IsValid)
            {
                var handler = _factory.Create<TCommand>();
                handler.Handle(command);
            }

            return aggregateResult;
        }

        private CommandValidationResult Validate<TCommand>(TCommand command) where TCommand : ICommand
        {
            var validators = _validatorFactory.Create<TCommand>();
            var aggregateResult = new CommandValidationResult();

            foreach (var validator in validators)
            {
                aggregateResult.Add(validator.Validate(command));
            }

            return aggregateResult;
        }
    }
}