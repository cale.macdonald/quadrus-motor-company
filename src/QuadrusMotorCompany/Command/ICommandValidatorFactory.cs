namespace QuadrusMotorCompany.Command
{
    using System.Collections.Generic;

    public interface ICommandValidatorFactory
    {
        IEnumerable<ICommandValidator<TCommand>> Create<TCommand>() where TCommand : ICommand;
    }
}