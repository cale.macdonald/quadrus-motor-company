﻿namespace QuadrusMotorCompany.Command
{
    public interface ICommandBus
    {
        CommandValidationResult Execute<TCommand>(TCommand command) where TCommand : ICommand;
    }
}