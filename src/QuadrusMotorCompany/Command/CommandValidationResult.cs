﻿namespace QuadrusMotorCompany.Command
{
    using System.Collections.Generic;
    using System.Linq;

    public class CommandValidationResult
    {
        private readonly List<CommandValidationError> _errors;

        public CommandValidationResult()
        {
            _errors = new List<CommandValidationError>();
        }

        public IList<CommandValidationError> Errors => _errors;

        public bool IsValid => !Errors.Any();

        public void Add(CommandValidationResult result)
        {
            _errors.AddRange(result.Errors);
        }
    }
}
