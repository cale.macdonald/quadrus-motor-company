﻿namespace QuadrusMotorCompany.Event
{
    public class EventBus : IEventBus
    {
        private readonly IEventHandlerFactory _factory;

        public EventBus(IEventHandlerFactory factory)
        {
            _factory = factory;
        }

        public void Publish<TEvent>(TEvent @event) where TEvent : IEvent
        {
            var handler = _factory.Create<TEvent>();
            handler.Handle(@event);
        }
    }
}
