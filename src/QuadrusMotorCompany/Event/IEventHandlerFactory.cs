﻿namespace QuadrusMotorCompany.Event
{
    public interface IEventHandlerFactory
    {
        IEventHandler<TEvent> Create<TEvent>() where TEvent : IEvent;
    }
}