﻿namespace QuadrusMotorCompany.Core
{
    using QuadrusMotorCompany.Query;

    public class IsVehicleMakeAndModelUsedQuery : IQuery<bool>
    {
        public IsVehicleMakeAndModelUsedQuery(string make, string model)
        {
            Make = make;
            Model = model;
        }

        public string Make { get; }

        public string Model { get; }
    }
}
