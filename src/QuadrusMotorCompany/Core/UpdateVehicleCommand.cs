﻿namespace QuadrusMotorCompany.Core
{
    using QuadrusMotorCompany.Command;

    public class UpdateVehicleCommand : ICommand
    {
        public UpdateVehicleCommand(Vehicle vehicle)
        {
            Vehicle = vehicle;
        }

        public Vehicle Vehicle { get; }
    }
}
