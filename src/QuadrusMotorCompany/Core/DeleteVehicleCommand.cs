﻿namespace QuadrusMotorCompany.Core
{
    using QuadrusMotorCompany.Command;

    public class DeleteVehicleCommand : ICommand
    {
        public DeleteVehicleCommand(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
