﻿namespace QuadrusMotorCompany.Core
{
    using QuadrusMotorCompany.Query;

    public class VehicleImageQuery : IQuery<string>
    {
        public VehicleImageQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
