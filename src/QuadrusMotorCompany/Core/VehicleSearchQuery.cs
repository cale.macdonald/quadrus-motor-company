﻿namespace QuadrusMotorCompany.Core
{
    using System.Collections.Generic;

    using QuadrusMotorCompany.Query;

    public class VehicleSearchQuery : IQuery<IReadOnlyCollection<Vehicle>>
    {
        public VehicleSearchQuery(string query)
        {
            Query = query;
        }

        public string Query { get; }
    }
}