﻿namespace QuadrusMotorCompany.Core
{
    using QuadrusMotorCompany.Query;

    public class VehicleDetailsQuery : IQuery<Vehicle>
    {
        public VehicleDetailsQuery(string make, string model)
        {
            Make = make;
            Model = model;
        }

        public string Make { get; }

        public string Model { get; }
    }
}
