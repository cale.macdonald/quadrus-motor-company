﻿namespace QuadrusMotorCompany.Core
{
    using System.Collections.Generic;

    using QuadrusMotorCompany.Query;

    public class VehicleQuery : IQuery<IReadOnlyCollection<Vehicle>>
    {
        public VehicleQuery()
        {
        }

        public VehicleQuery(string brand)
        {
            Brand = brand;
        }

        public string Brand { get; }
    }
}
