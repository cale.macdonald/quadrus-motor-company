﻿namespace QuadrusMotorCompany.Core
{
    using QuadrusMotorCompany.Command;

    public class AddVehicleCommand : ICommand
    {
        public AddVehicleCommand(Vehicle vehicle)
        {
            Vehicle = vehicle;
        }

        public Vehicle Vehicle { get; }
    }
}
