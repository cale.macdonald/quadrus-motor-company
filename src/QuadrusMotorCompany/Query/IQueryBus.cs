﻿namespace QuadrusMotorCompany.Query
{
    public interface IQueryBus
    {
        TResult Query<TResult>(IQuery<TResult> query);
    }
}