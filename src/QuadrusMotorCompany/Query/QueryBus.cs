namespace QuadrusMotorCompany.Query
{
    using System;

    public class QueryBus : IQueryBus
    {
        private const string HandleMethod = "Handle";
        private readonly IQueryHandlerFactory _factory;

        public QueryBus(IQueryHandlerFactory factory)
        {
            _factory = factory;
        }

        public TResult Query<TResult>(IQuery<TResult> query)
        {
            var queryType = query.GetType();
            var resultType = typeof(TResult);

            var handler = GetHandler(queryType, resultType);

            var method = handler.GetType().GetMethod(HandleMethod, new[] { queryType });
            var result = method.Invoke(handler, new object[] { query });

            return result is TResult ? (TResult)result : default(TResult);
        }

        private object GetHandler(Type queryType, Type resultType)
        {
            var handlerType = typeof(IQueryHandler<,>).MakeGenericType(queryType, resultType);
            var handler = _factory.Create(handlerType);
            return handler;
        }
    }
}