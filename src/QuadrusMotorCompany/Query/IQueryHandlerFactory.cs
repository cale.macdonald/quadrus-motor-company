namespace QuadrusMotorCompany.Query
{
    using System;

    public interface IQueryHandlerFactory
    {
        object Create(Type handlerType);
    }
}