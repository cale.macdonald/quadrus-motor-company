namespace QuadrusMotorCompany.Data
{
    using System;
    using System.Data.Entity;

    public interface IVehicleContext : IDisposable
    {
        IDbSet<Vehicle> Vehicles { get; }

        int SaveChanges();
    }
}