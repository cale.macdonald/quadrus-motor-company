﻿namespace QuadrusMotorCompany.Data.CommandHandlers
{
    using AutoMapper;

    using QuadrusMotorCompany.Command;
    using QuadrusMotorCompany.Core;

    using Vehicle = QuadrusMotorCompany.Data.Vehicle;

    public class AddVehicleCommandHandler : ICommandHandler<AddVehicleCommand>
    {
        private readonly IVehicleContext _context;
        private readonly IMapper _mapper;

        public AddVehicleCommandHandler(IVehicleContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Handle(AddVehicleCommand command)
        {
            var entity = _mapper.Map<Vehicle>(command.Vehicle);
            _context.Vehicles.Add(entity);
            _context.SaveChanges();
        }
    }
}
