﻿namespace QuadrusMotorCompany.Data.CommandHandlers
{
    using QuadrusMotorCompany.Command;
    using QuadrusMotorCompany.Core;

    public class DeleteVehicleCommandHandler : ICommandHandler<DeleteVehicleCommand>
    {
        private readonly IVehicleContext _context;

        public DeleteVehicleCommandHandler(IVehicleContext context)
        {
            _context = context;
        }

        public void Handle(DeleteVehicleCommand command)
        {
            var vehicle = _context.Vehicles.Find(command.Id);

            if (vehicle == null)
            {
                return;
            }

            _context.Vehicles.Remove(vehicle);
            _context.SaveChanges();
        }
    }
}
