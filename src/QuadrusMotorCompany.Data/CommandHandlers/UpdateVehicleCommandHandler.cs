﻿namespace QuadrusMotorCompany.Data.CommandHandlers
{
    using AutoMapper;

    using QuadrusMotorCompany.Command;
    using QuadrusMotorCompany.Core;

    public class UpdateVehicleCommandHandler : ICommandHandler<UpdateVehicleCommand>
    {
        private readonly IVehicleContext _context;
        private readonly IMapper _mapper;

        public UpdateVehicleCommandHandler(IVehicleContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Handle(UpdateVehicleCommand command)
        {
            var vehicle = _context.Vehicles.Find(command.Vehicle.Id);
            _mapper.Map(command.Vehicle, vehicle);

            _context.SaveChanges();
        }
    }
}
