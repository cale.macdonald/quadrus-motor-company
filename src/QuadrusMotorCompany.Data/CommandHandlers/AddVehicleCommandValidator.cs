﻿namespace QuadrusMotorCompany.Data.CommandHandlers
{
    using QuadrusMotorCompany.Command;
    using QuadrusMotorCompany.Core;
    using QuadrusMotorCompany.Query;

    public class AddVehicleCommandValidator : ICommandValidator<AddVehicleCommand>
    {
        private readonly IQueryBus _queryBus;

        public AddVehicleCommandValidator(IQueryBus queryBus)
        {
            _queryBus = queryBus;
        }

        public CommandValidationResult Validate(AddVehicleCommand command)
        {
            var isUsed = _queryBus.Query(new IsVehicleMakeAndModelUsedQuery(command.Vehicle.Make, command.Vehicle.Model));

            var result = new CommandValidationResult();
            if (isUsed)
            {
                result.Errors.Add(new CommandValidationError("make", "The make and model combination is already used"));
            }

            return result;
        }
    }
}
