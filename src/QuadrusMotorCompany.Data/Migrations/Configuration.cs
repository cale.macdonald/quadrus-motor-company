namespace QuadrusMotorCompany.Data.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.IO;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<VehicleContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(VehicleContext context)
        {
            var vehicles = LoadVehicles();
            foreach (var vehicle in vehicles)
            {
                context.Vehicles.AddOrUpdate(x => x.Id, vehicle);
            }

            context.SaveChanges();
        }

        private IEnumerable<Vehicle> LoadVehicles()
        {
            var json = LoadFileContents("QuadrusMotorCompany.Data.Seed.vehicles.json");
            
            return JsonConvert.DeserializeObject<List<Vehicle>>(json, new VehicleJsonConverter());
        }

        private string LoadFileContents(string resourceName)
        {
            var assembly = GetType().Assembly;

            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        private class VehicleJsonConverter : JsonConverter
        {
            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                var jo = JObject.Load(reader);

                var id = (int)jo["id"];
                var make = (string)jo["make"];
                var model = (string)jo["model"];
                var price = (decimal)jo["price"];
                var img = (string)jo["img"];
                var description = (string)jo["description"];

                return new Vehicle
                {
                    Id = id,
                    Make = make,
                    Model = model,
                    Price = price,
                    Description = description,
                    ImagePath = img.Replace("img/", "")
                };
            }

            public override bool CanConvert(Type objectType)
            {
                return objectType == typeof(Vehicle);
            }
        }
    }
}
