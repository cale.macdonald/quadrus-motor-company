﻿namespace QuadrusMotorCompany.Data
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;

    public class VehicleContext : DbContext, IVehicleContext
    {
        public VehicleContext()
            : base("Vehicle")
        {
            Database.SetInitializer<VehicleContext>(null);
        }

        public IDbSet<Vehicle> Vehicles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vehicle>()
                .Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
