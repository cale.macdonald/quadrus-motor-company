﻿namespace QuadrusMotorCompany.Data.Mappings
{
    using AutoMapper;
    
    public class VehicleMappingProfile : Profile
    {
        public VehicleMappingProfile()
        {
            CreateMap<Vehicle, Core.Vehicle>();

            CreateMap<Core.Vehicle, Vehicle>()
                .ForMember(dest => dest.Id, opt => opt.Ignore());
        }
    }
}
