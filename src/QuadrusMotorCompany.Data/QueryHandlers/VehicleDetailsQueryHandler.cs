﻿namespace QuadrusMotorCompany.Data.QueryHandlers
{
    using System.Linq;

    using AutoMapper;

    using QuadrusMotorCompany.Core;
    using QuadrusMotorCompany.Query;

    public class VehicleDetailsQueryHandler : IQueryHandler<VehicleDetailsQuery, Vehicle>
    {
        private readonly IVehicleContext _context;
        private readonly IMapper _mapper;

        public VehicleDetailsQueryHandler(IVehicleContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Vehicle Handle(VehicleDetailsQuery query)
        {
            var vehicle = _context.Vehicles.SingleOrDefault(v => v.Make == query.Make && v.Model == query.Model);
            return _mapper.Map<Vehicle>(vehicle);
        }
    }
}
