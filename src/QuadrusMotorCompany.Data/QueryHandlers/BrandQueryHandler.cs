﻿namespace QuadrusMotorCompany.Data.QueryHandlers
{
    using System.Collections.Generic;
    using System.Linq;

    using QuadrusMotorCompany.Core;
    using QuadrusMotorCompany.Query;

    public class BrandQueryHandler : IQueryHandler<BrandQuery, IReadOnlyDictionary<string, int>>
    {
        private readonly IVehicleContext _context;

        public BrandQueryHandler(IVehicleContext context)
        {
            _context = context;
        }

        public IReadOnlyDictionary<string, int> Handle(BrandQuery query)
        {
            return _context.Vehicles.GroupBy(v => v.Make).ToDictionary(g => g.Key, g => g.Count());
        }
    }
}