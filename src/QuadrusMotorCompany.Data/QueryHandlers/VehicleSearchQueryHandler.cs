﻿namespace QuadrusMotorCompany.Data.QueryHandlers
{
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using QuadrusMotorCompany.Core;
    using QuadrusMotorCompany.Query;

    public class VehicleSearchQueryHandler : IQueryHandler<VehicleSearchQuery, IReadOnlyCollection<Vehicle>>
    {
        private readonly IVehicleContext _context;
        private readonly IMapper _mapper;

        public VehicleSearchQueryHandler(IVehicleContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IReadOnlyCollection<Vehicle> Handle(VehicleSearchQuery query)
        {
            var vehicles = from v in _context.Vehicles
                where v.Model.Contains(query.Query) || v.Make.Contains(query.Query) || v.Description.Contains(query.Query)
                select v;

            return _mapper.Map<IReadOnlyCollection<Vehicle>>(vehicles.ToList());
        }
    }
}