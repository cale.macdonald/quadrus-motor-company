﻿namespace QuadrusMotorCompany.Data.QueryHandlers
{
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using QuadrusMotorCompany.Core;
    using QuadrusMotorCompany.Query;
    
    public class VehicleQueryHandler : IQueryHandler<VehicleQuery, IReadOnlyCollection<Vehicle>>
    {
        private readonly IVehicleContext _context;
        private readonly IMapper _mapper;

        public VehicleQueryHandler(IVehicleContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IReadOnlyCollection<Vehicle> Handle(VehicleQuery query)
        {
            var vehicles = _context.Vehicles.AsQueryable();

            if (!string.IsNullOrEmpty(query.Brand))
            {
                vehicles = vehicles.Where(v => v.Make == query.Brand);
            }

            return _mapper.Map<IReadOnlyCollection<Vehicle>>(vehicles.ToList());
        }
    }
}
