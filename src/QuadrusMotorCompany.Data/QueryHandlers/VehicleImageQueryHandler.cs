﻿namespace QuadrusMotorCompany.Data.QueryHandlers
{
    using QuadrusMotorCompany.Core;
    using QuadrusMotorCompany.Query;

    public class VehicleImageQueryHandler : IQueryHandler<VehicleImageQuery, string>
    {
        private readonly IVehicleContext _context;

        public VehicleImageQueryHandler(IVehicleContext context)
        {
            _context = context;
        }

        public string Handle(VehicleImageQuery query)
        {
            var vehicle = _context.Vehicles.Find(query.Id);
            return vehicle?.ImagePath;
        }
    }
}
