﻿namespace QuadrusMotorCompany.Data.QueryHandlers
{
    using System.Linq;

    using QuadrusMotorCompany.Core;
    using QuadrusMotorCompany.Query;

    public class IsVehicleMakeAndModelUsedQueryHandler : IQueryHandler<IsVehicleMakeAndModelUsedQuery, bool>
    {
        private readonly IVehicleContext _context;

        public IsVehicleMakeAndModelUsedQueryHandler(IVehicleContext context)
        {
            _context = context;
        }

        public bool Handle(IsVehicleMakeAndModelUsedQuery query)
        {
            return _context.Vehicles.Any(v => v.Make == query.Make && v.Model == query.Model);
        }
    }
}
