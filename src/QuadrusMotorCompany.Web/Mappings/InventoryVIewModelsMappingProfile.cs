﻿namespace QuadrusMotorCompany.Web.Mappings
{
    using AutoMapper;

    using QuadrusMotorCompany.Core;
    using QuadrusMotorCompany.Web.Models.Inventory;

    public class InventoryVIewModelsMappingProfile : Profile
    {
        public InventoryVIewModelsMappingProfile()
        {
            CreateMap<Vehicle, VehicleInventoryViewModel>();
            CreateMap<Vehicle, VehicleEditModel>();

            CreateMap<VehicleEditModel, Vehicle>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Make, opt => opt.Ignore())
                .ForMember(dest => dest.Model, opt => opt.Ignore());

            CreateMap<VehicleAddModel, Vehicle>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.ImagePath, opt => opt.MapFrom(src => src.ImagePath.FileName))
                .ForMember(dest => dest.Model, opt => opt.MapFrom(src => src.VehicleModel));
        }
    }
}