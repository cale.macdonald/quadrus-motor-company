﻿
namespace QuadrusMotorCompany.Web.Mappings
{
    using AutoMapper;

    using System.Collections.Generic;

    using QuadrusMotorCompany.Web.Models;

    public class BrandViewModelsMappingProfile : Profile
    {
        public BrandViewModelsMappingProfile()
        {
            CreateMap<KeyValuePair<string, int>, BrandListingViewModel>()
                .ConvertUsing(pair => new BrandListingViewModel { Name = pair.Key, Count = pair.Value });
        }
    }
}