﻿namespace QuadrusMotorCompany.Web.Mappings
{
    using AutoMapper;
    using QuadrusMotorCompany.Core;
    using QuadrusMotorCompany.Web.Models.Gallery;
    using QuadrusMotorCompany.Web.Models.Vehicle;

    public class VehicleViewModelsMappingProfile : Profile
    {
        public VehicleViewModelsMappingProfile()
        {
            CreateMap<Vehicle, VehicleIndexModel>();
            CreateMap<Vehicle, VehicleDetailsModel>();
        }
    }
}