﻿namespace QuadrusMotorCompany.Web
{
    using System;

    using Autofac;

    using QuadrusMotorCompany.Query;

    public class QueryHandlerFactory : IQueryHandlerFactory, IDisposable
    {
        private readonly ILifetimeScope _scope;

        public QueryHandlerFactory(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public object Create(Type handlerType)
        {
            return _scope.Resolve(handlerType);
        }

        public void Dispose()
        {
            _scope?.Dispose();
        }
    }
}