﻿namespace QuadrusMotorCompany.Web
{
    using System;
    using System.Collections.Generic;

    using Autofac;

    using QuadrusMotorCompany.Command;

    public class CommandValidatorFactory : ICommandValidatorFactory, IDisposable
    {
        private readonly ILifetimeScope _scope;

        public CommandValidatorFactory(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public IEnumerable<ICommandValidator<TCommand>> Create<TCommand>() where TCommand : ICommand
        {
            return _scope.Resolve<IEnumerable<ICommandValidator<TCommand>>>();
        }

        public void Dispose()
        {
            _scope?.Dispose();
        }
    }
}