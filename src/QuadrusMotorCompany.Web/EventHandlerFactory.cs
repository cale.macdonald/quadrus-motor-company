﻿namespace QuadrusMotorCompany.Web
{
    using System;

    using Autofac;
    using QuadrusMotorCompany.Event;

    public class EventHandlerFactory : IEventHandlerFactory, IDisposable
    {
        private readonly ILifetimeScope _scope;

        public EventHandlerFactory(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public IEventHandler<TEvent> Create<TEvent>() where TEvent : IEvent
        {
            return _scope.Resolve<IEventHandler<TEvent>>();
        }

        public void Dispose()
        {
            _scope?.Dispose();
        }
    }
}