﻿namespace QuadrusMotorCompany.Web.Models.Vehicle
{
    public class VehicleDetailsModel
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Description { get; set; }
    }
}