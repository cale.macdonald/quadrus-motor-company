﻿namespace QuadrusMotorCompany.Web.Models.Inventory
{
    using System.ComponentModel.DataAnnotations;
    using System.Web;

    public class VehicleAddModel
    {
        [Required]
        public string Make { get; set; }

        [Required]
        [Display(Name = "Model")]
        public string VehicleModel { get; set; }

        [Required]
        [Range(1, double.MaxValue, ErrorMessage = "Price has to be more than $1")]
        public decimal Price { get; set; }

        [Required]
        [Display(Name = "Image")]
        public HttpPostedFileBase ImagePath { get; set; }

        public string Description { get; set; }
    }
}