﻿namespace QuadrusMotorCompany.Web.Models.Inventory
{
    using System.ComponentModel.DataAnnotations;

    public class VehicleEditModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Make { get; set; }

        [Required]
        public string Model { get; set; }

        [Required]
        [Range(0, double.MaxValue)]
        public decimal Price { get; set; }
        
        [Required]
        public string ImagePath { get; set; }

        public string Description { get; set; }
    }
}