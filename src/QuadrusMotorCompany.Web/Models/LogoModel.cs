﻿namespace QuadrusMotorCompany.Web.Models
{
    public class LogoModel
    {
        public int Width { get; set; }

        public int Height { get; set; }
    }
}