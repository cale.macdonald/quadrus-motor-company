﻿namespace QuadrusMotorCompany.Web.Models.Gallery
{
    public class VehicleIndexModel
    {
        public int Id { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }
        
        public decimal Price { get; set; }
    }
}