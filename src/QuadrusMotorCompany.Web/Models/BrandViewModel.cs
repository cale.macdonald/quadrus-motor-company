﻿namespace QuadrusMotorCompany.Web.Models
{
    public class BrandListingViewModel
    {
        public string Name { get; set; }

        public int Count { get; set; }
    }
}