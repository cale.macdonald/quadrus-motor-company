﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QuadrusMotorCompany.Web.Startup))]
namespace QuadrusMotorCompany.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
