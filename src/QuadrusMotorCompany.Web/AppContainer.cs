﻿namespace QuadrusMotorCompany.Web
{
    using System.Collections.Generic;
    using System.Reflection;
    using System.Web.Mvc;
    using Autofac;
    using Autofac.Integration.Mvc;
    using AutoMapper;

    using QuadrusMotorCompany.Command;
    using QuadrusMotorCompany.Data;
    using QuadrusMotorCompany.Event;
    using QuadrusMotorCompany.Query;

    public static class AppContainer
    {
        public static void Bootstrap()
        {
            var builder = new ContainerBuilder();

            RegisterComponents(builder);
            
            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }

        private static void RegisterComponents(ContainerBuilder builder)
        {
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            RegisterAutoMapper(builder);
            RegisterEvents(builder);
            RegisterQueries(builder);
            RegisterRepositories(builder);
            RegisterCommands(builder);
        }

        private static void RegisterAutoMapper(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(VehicleContext).Assembly).AssignableTo(typeof(Profile)).As<Profile>();
            builder.RegisterAssemblyTypes(typeof(Startup).Assembly).AssignableTo(typeof(Profile)).As<Profile>();

            builder.Register(c => new MapperConfiguration(cfg =>
            {
                foreach (var profile in c.Resolve<IEnumerable<Profile>>())
                {
                    cfg.AddProfile(profile);
                }
            })).AsSelf().SingleInstance();

            builder.Register(c => c.Resolve<MapperConfiguration>().CreateMapper(c.Resolve))
                .As<IMapper>()
                .InstancePerLifetimeScope();
        }

        private static void RegisterEvents(ContainerBuilder builder)
        {
            builder.RegisterType<EventHandlerFactory>().As<IEventHandlerFactory>().InstancePerRequest();
            builder.RegisterType<EventBus>().As<IEventBus>().InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(IEvent).Assembly)
                .Where(t => t.Name.EndsWith("Event"))
                .AsImplementedInterfaces()
                .InstancePerRequest();
        }

        private static void RegisterQueries(ContainerBuilder builder)
        {
            builder.RegisterType<QueryHandlerFactory>().As<IQueryHandlerFactory>().InstancePerRequest();
            builder.RegisterType<QueryBus>().As<IQueryBus>().InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(IVehicleContext).Assembly)
                .Where(t => t.Name.EndsWith("QueryHandler"))
                .AsImplementedInterfaces()
                .InstancePerRequest();
        }

        private static void RegisterRepositories(ContainerBuilder builder)
        {
            builder.RegisterType<VehicleContext>().As<IVehicleContext>().InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(IVehicleContext).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerRequest();
        }

        private static void RegisterCommands(ContainerBuilder builder)
        {
            builder.RegisterType<CommandBus>().As<ICommandBus>().InstancePerRequest();
            builder.RegisterType<CommandHandlerFactory>().As<ICommandHandlerFactory>().InstancePerRequest();
            builder.RegisterType<CommandValidatorFactory>().As<ICommandValidatorFactory>().InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(IVehicleContext).Assembly)
                .Where(t => t.Name.EndsWith("CommandHandler"))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(IVehicleContext).Assembly)
                .Where(t => t.Name.EndsWith("CommandValidator"))
                .AsImplementedInterfaces()
                .InstancePerRequest();
        }
    }
}