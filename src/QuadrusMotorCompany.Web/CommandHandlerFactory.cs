﻿namespace QuadrusMotorCompany.Web
{
    using System;

    using Autofac;

    using QuadrusMotorCompany.Command;

    public class CommandHandlerFactory : ICommandHandlerFactory, IDisposable
    {
        private readonly ILifetimeScope _scope;

        public CommandHandlerFactory(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public ICommandHandler<TCommand> Create<TCommand>() where TCommand : ICommand
        {
            return _scope.Resolve<ICommandHandler<TCommand>>();
        }

        public void Dispose()
        {
            _scope?.Dispose();
        }
    }
}