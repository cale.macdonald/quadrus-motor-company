﻿namespace QuadrusMotorCompany.Web.Controllers
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Web;
    using System.Web.Mvc;

    using AutoMapper;

    using QuadrusMotorCompany.Command;
    using QuadrusMotorCompany.Core;
    using QuadrusMotorCompany.Query;
    using QuadrusMotorCompany.Web.Models.Inventory;

    [Authorize]
    public class InventoryController : Controller
    {
        private readonly IQueryBus _queryBus;
        private readonly IMapper _mapper;
        private readonly ICommandBus _commandBus;

        public InventoryController(IQueryBus queryBus, IMapper mapper, ICommandBus commandBus)
        {
            _queryBus = queryBus;
            _mapper = mapper;
            _commandBus = commandBus;
        }

        [Route("~/inventory/vehicle/add")]
        public ActionResult Add()
        {
            return View(new VehicleAddModel());
        }

        [HttpPost]
        [Route("~/inventory/vehicle/add")]
        public ActionResult Add(VehicleAddModel model)
        {
            if (ModelState.IsValid)
            {
                var vehicle = _mapper.Map<Vehicle>(model);

                var result = _commandBus.Execute(new AddVehicleCommand(vehicle));
                if (result.IsValid)
                {
                    var image = ReadBytes(model.ImagePath);

                    var root = ConfigurationManager.AppSettings["VehicleImagePathRoot"];
                    var path = Server.MapPath(Path.Combine(root, model.ImagePath.FileName));

                    System.IO.File.WriteAllBytes(path, image);

                    return RedirectToAction("Manage");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(error.Property, error.Message);
                }
            }

            return View(model);
        }

        [Route("~/inventory/vehicle/{id}/delete")]
        public ActionResult Delete(int id)
        {
            _commandBus.Execute(new DeleteVehicleCommand(id));
            return RedirectToAction("Manage");
        }

        [Route("~/inventory/vehicle/{make}/{model}/edit")]
        public ActionResult Edit(string make, string model)
        {
            var vehicle = _queryBus.Query(new VehicleDetailsQuery(make.Replace("_", " "), model.Replace("_", " ")));
            var viewModel = _mapper.Map<VehicleEditModel>(vehicle);

            return View(viewModel);
        }

        [HttpPost]
        [Route("~/inventory/vehicle/{make}/{model}/edit")]
        public ActionResult Edit(string make, string model, VehicleEditModel viewModel)
        {
            //todo ensure that the mae and model match what the view model contains
            if (ModelState.IsValid)
            {
                var vehicle = _queryBus.Query(new VehicleDetailsQuery(make, model));
                _mapper.Map(viewModel, vehicle);

                _commandBus.Execute(new UpdateVehicleCommand(vehicle));
                return RedirectToAction("Manage");
            }

            return View(viewModel);
        }

        [Route("~/inventory/manage")]
        public ActionResult Manage()
        {
            var vehicles = _queryBus.Query(new VehicleQuery());
            var models = _mapper.Map<IList<VehicleInventoryViewModel>>(vehicles);

            return View(models);
        }

        private byte[] ReadBytes(HttpPostedFileBase file)
        {
            byte[] contents;

            using (var stream = new MemoryStream())
            {
                file.InputStream.CopyTo(stream);
                contents = stream.ToArray();
            }

            return contents;
        }
    }
}