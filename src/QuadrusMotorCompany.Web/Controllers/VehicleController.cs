﻿namespace QuadrusMotorCompany.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Web.Mvc;

    using AutoMapper;

    using QuadrusMotorCompany.Core;
    using QuadrusMotorCompany.Query;
    using QuadrusMotorCompany.Web.Models.Gallery;
    using QuadrusMotorCompany.Web.Models.Vehicle;

    using Img = System.Drawing.Image;

    public class VehicleController : Controller
    {
        private readonly IQueryBus _queryBus;
        private readonly IMapper _mapper;

        public VehicleController(IQueryBus queryBus, IMapper mapper)
        {
            _queryBus = queryBus;
            _mapper = mapper;
        }

        [ChildActionOnly]
        public ActionResult Index()
        {
            var vehicles = _queryBus.Query(new VehicleQuery());
            var models = _mapper.Map<IList<VehicleIndexModel>>(vehicles);

            return PartialView("_Index", models);
        }

        [Route("~/vehicle/{make}/{model}")]
        public ActionResult Details(string make, string model)
        {
            var vehicle = _queryBus.Query(new VehicleDetailsQuery(make.Replace("_", " "), model.Replace("_", " ")));

            var viewModel = _mapper.Map<VehicleDetailsModel>(vehicle);
            return View(viewModel);
        }

        [Route("~/vehicle/{id}/image")]
        public ActionResult Image(int id, int? width, int? height)
        {
            var vehicleImage = _queryBus.Query(new VehicleImageQuery(id));

            var root = Server.MapPath(ConfigurationManager.AppSettings["VehicleImagePathRoot"]);
            var path = Path.Combine(root, vehicleImage);

            var vehcileImage = Img.FromFile(path);
            if (width.HasValue && height.HasValue)
            {
                vehcileImage = ScaleImage(vehcileImage, width.Value, height.Value);
            }
            
            var byteArray = ConvertToByteArray(vehcileImage);

            return File(byteArray, "image/png");
        }

        private static Img ScaleImage(Img image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(newImage))
            {
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            return newImage;
        }

        private static byte[] ConvertToByteArray(Img image)
        {
            byte[] bytes;

            using (var stream = new MemoryStream())
            {
                image.Save(stream, ImageFormat.Png);
                bytes = stream.ToArray();
            }

            return bytes;
        }
    }
}