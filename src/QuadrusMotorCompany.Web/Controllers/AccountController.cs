﻿namespace QuadrusMotorCompany.Web.Controllers
{
    using System.Web;
    using System.Web.Mvc;
    
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        [Authorize]
        public ActionResult Logout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();

            return RedirectToAction("Index", "Home");
        }
    }
}