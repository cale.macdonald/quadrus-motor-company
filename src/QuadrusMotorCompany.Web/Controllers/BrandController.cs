﻿namespace QuadrusMotorCompany.Web.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    using AutoMapper;

    using QuadrusMotorCompany.Core;
    using QuadrusMotorCompany.Query;
    using QuadrusMotorCompany.Web.Models;

    public class BrandController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IQueryBus _queryBus;

        public BrandController(IMapper mapper, IQueryBus queryBus)
        {
            _mapper = mapper;
            _queryBus = queryBus;
        }

        [ChildActionOnly]
        public ActionResult Index()
        {
            var brands = _queryBus.Query(new BrandQuery());
            var models = _mapper.Map<IList<BrandListingViewModel>>(brands);

            return PartialView("_Index", models);
        }
    }
}