﻿namespace QuadrusMotorCompany.Web.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using AutoMapper;
    using QuadrusMotorCompany.Web.Models;
    using QuadrusMotorCompany.Web.Models.Gallery;
    using QuadrusMotorCompany.Core;
    using QuadrusMotorCompany.Query;

    public class GalleryController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IQueryBus _queryBus;

        public GalleryController(IMapper mapper, IQueryBus queryBus)
        {
            _mapper = mapper;
            _queryBus = queryBus;
        }

        [Route("~/gallery")]
        public ActionResult Index()
        {
            return View();
        }

        [Route("~/gallery/brands/{brandName}")]
        public ActionResult ByBrand(string brandName)
        {
            var vehicles = _queryBus.Query(new VehicleQuery(brandName));
            var model = _mapper.Map<IList<VehicleIndexModel>>(vehicles);

            ViewBag.SearchText = brandName;

            return View(model);
        }


        [Route("~/gallery/search/")]
        public ActionResult Search(string q)
        {
            var vehicles = _queryBus.Query(new VehicleSearchQuery(q));
            var model = _mapper.Map<IList<VehicleIndexModel>>(vehicles);
            
            ViewBag.SearchText = q;

            return View(model);
        }
    }
}