﻿namespace QuadrusMotorCompany.Web.Controllers
{
    using System.Web.Mvc;

    using QuadrusMotorCompany.Web.Models;

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult Logo(int width, int height)
        {
            return PartialView("_Logo", new LogoModel { Width = width, Height = height });
        }
    }
}